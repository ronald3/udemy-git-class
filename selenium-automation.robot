*** Setting ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${navegador}    Chrome
${homepage}     automationpractice.com/index.php
${sheme}        http
${testUrl}      ${sheme}://${homepage}

*** Keywords ***
Abrir homepage
    Open Browser    ${testUrl}      ${navegador}

*** Test Cases ***
C001 Hacer click en Contenedores
    Abrir homepage
    Set Global Variable     @{nombresDeContenedores}       //*[@id="homefeatured"]/li[1]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[2]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[3]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[4]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[5]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[6]/div/div[2]/h5/a       //*[@id="homefeatured"]/li[7]/div/div[2]/h5/a
    FOR    ${nombreDeContenedor}   IN      @{nombresDeContenedores}
       Click Element       xpath=${nombreDeContenedor}
       Wait Until Element Is Visible       xpath=//*[@id="bigpic"]
       Click Element       xpath=//*[@id="header_logo"]/a/img
    END
    Close Browser

C002 Caso de Prueba Nuevo
    Open homepage

